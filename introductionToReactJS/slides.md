
layout:true

.footer[Introduction to React | The Hatch Team]

---


name: Title
class: center, middle, title

# An Introduction to React

![React](reactjs.png)

---

name: Agenda

# Agenda

1. The Framework
2. Concepts
3. JSX
4. Component Structure, State & Props
5. ECMAScript2015

---
name: Framework

# The Framework

### Developed by facebook & Instagram

### Used by Netflix, Imgur, Airbnb, Feedly, atlassian

### As of January 2015, React and React Native are Facebook's top two open-source projects by number of stars on GitHub, and React is the 9th most starred project of all time on GitHub.

---
```
const TodoList = React.createClass({
  render: function() {
    const createItem = (item) => <li key={item.id}>{item.text}</li>;
    return <ul>{this.props.items.map(createItem)}</ul>;
  }
});
const TodoApp = React.createClass({
  getInitialState: function() {
    return {items: [], text: ''};
  },
  onChange: function(e) {
    this.setState({text: e.target.value});
  },
  handleSubmit: function(e) {
    const newItem = [{text: this.state.text, id: Date.now()}];
    const nextItems = this.state.items.concat(newItem);
    this.setState({items: nextItems, text: ''});
  },
  render: function() {
    return (
      <div>
        <h3>TODO</h3>
        <TodoList items={this.state.items} />
        <form onSubmit={this.handleSubmit}>
          <input onChange={this.onChange} value={this.state.text} />
          <button>{'Add #' + (this.state.items.length + 1)}</button>
        </form>
      </div>
    );
  }
});
ReactDOM.render(<TodoApp />, mountNode);
```
---

name: Concepts

# Concepts : 1. Just the UI

*«A JavaScript Library for building User Interfaces»*

* No full-fledged JavaScript MV* Framework
* Additional Libraries for publish/subscribe, data-storage
* The **V** in **MVC**

.width-500[![ui](firefox-everywhere.png)]

---

# Concepts :  2. Virtual DOM

* DOM abstracted
* Performance
* Server-Side Rendering

![virtual DOM](virtualdom.png)

---

# Concepts :  3. Data Flow

* One-way reactive data flow

![One-way data flow](data-flow.png)

---

name: JSX

# JSX

A XML-like syntax extension to ECMAScript

```javascript
const myComponent = <div className="super-div">This is a div</div>;
```

Will be transpiled to

```javascript
const myComponent = React.createElement(
  "div",
  { className: "super-div" },
  "This is a div"
);
```

### Experiment

Visit https://babeljs.io/repl/ in order to try out JSX

---

name: JSX

# JSX (2)

Attribute Expressions

```javascript
const isActive = true;
const myComponent = <div className={isActive ? 'active' : 'inactive'}>This is a div</div>;
```

Child Expressions

```javascript
const isActive = true;
const myComponent = (
  <div>
  {isActive && 
  <h1>Only shown when active</h1>
  }
  </div>
);
```

---

# Component Structure

```javascript
class MyView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }
  
  onButtonClick() {
    this.setState({
      someAttribute:'clicked'
    });
  }
  
  render() {
    return (
      <div className='my-view'>
        <button onClick={this.onButtonClick.bind(this)}>Click me</button>
        {this.state.someAttribute}
      </div>
    );
  }

}
```

---
name:ES2015_arrowFunctions

# ECMAScript2015 : Arrow Functions

```javascript
[1, 2, 3].map(num => num * 2)
// <- [2, 4, 6]
```

```javascript
[1, 2, 3].map(function (num) { return num * 2 })
// <- [2, 4, 6]
```

If we need to declare multiple (or zero) arguments, we’ll have to use parenthesis.
```javascript
[1, 2, 3, 4].map((num, index) => num * 2 + index)
// <- [2, 5, 8, 11]
```

You might want to have some other statements and not just an expression to return. In this case you’ll have to use braces notation.
```javascript
[1, 2, 3, 4].map(num => {
  var multiplier = 2 + num
  return num * multiplier
})
// <- [3, 8, 15, 24]
```

Source: ES6 Arrow Functions in Depth: https://ponyfoo.com/articles/es6-arrow-functions-in-depth

---
name:ES2015_destructuring

# ECMAScript2015 : Destructuring

```javascript
var foo = { bar: 'pony', baz: 3 }
var {bar, baz} = foo
console.log(bar)
// <- 'pony'
console.log(baz)
// <- 3
```

With aliases
```
var foo = { bar: 'pony', baz: 3 }
var {bar: a, baz: b} = foo
console.log(a)
// <- 'pony'
console.log(b)
// <- 3
```

Nested
```javascript
var foo = { bar: { deep: 'pony', dangerouslySetInnerHTML: 'lol' } }
var {bar: { deep, dangerouslySetInnerHTML: sure }} = foo
console.log(deep)
// <- 'pony'
console.log(sure)
// <- 'lol'
```


---
name:ES2015_destructuring2

# ECMAScript2015 : Destructuring (2)


```javascript
const myArrowFunction = ({a,b}) => {
  console.log(a);
  // <- 'A'
  console.log(b);
  // <- 'B'
};

myArrowFunction({a:'A',b:'B',c:'C'});
```

In ES5
```javascript
var myArrowFunction = function myArrowFunction(_ref) {
  var a = _ref.a;
  var b = _ref.b;

  console.log(a);
  console.log(b);
};
myArrowFunction({ a: 'A', b: 'B', c: 'C' });
```

Source: ES6 JavaScript Destructuring in Depth: https://ponyfoo.com/articles/es6-destructuring-in-depth

---

name:ES2015_var_let_const

# ECMAScript2015 : var let const

`var` is **function-scoped**. Gets pulled to the top of it's scope ("hoisting").
.row[
.six.columns[
```javascript
function areTheyAwesome (name) {
  if (name === 'nico') {
    var awesome = true
  }
  return awesome
}
```
]
.six.columns[
```javascript
function areTheyAwesome (name) {
  var awesome
  if (name === 'nico') {
    awesome = true
  }
  return awesome
}
```
]
]

`let` **is block-scoped**.
.row[
.six.columns[
```javascript
function areTheyAwesomeLet (name) {
  if (name === 'nico') {
    let awesome = true
  }
  return awesome
}
```
]

.six.columns[
```javascript
function areTheyAwesomeLet (name) {
  if (name === 'nico') {
    var _awesome = true
  }
  return awesome
}
```
]

]

---
name:ES2015_var_let_const2

# ECMAScript2015 : var let const (2)

* `const` is also block-scoped
* `const` variables must be declared using an initializer
* `const` variables can only be assigned to once, in said initializer
* `const` variables don’t make the assigned value immutable

```javascript
const cool = { people: ['you', 'me', 'tesla', 'musk'] }
cool = {}
// <- "cool" is read-only
```

```javascript
const cool = { people: ['you', 'me', 'tesla', 'musk'] }
cool.people.push('berners-lee')
console.log(cool)
// <- { people: ['you', 'me', 'tesla', 'musk', 'berners-lee'] }
```

Source: ES6 Let, Const and the “Temporal Dead Zone” (TDZ) in Depth: https://ponyfoo.com/articles/es6-let-const-and-temporal-dead-zone-in-depth



---

# Exercises

1. add a search input to specify a dynamic station instead of "Stadelhofen" (solution: git checkout solution_1)
2. extract the search input to an own component (solution: git checkout solution_2)
3. user can click a station -> next departures are queried and displayed  (solution: git checkout solution_3)
   1. API: http://transport.opendata.ch/docs.html#stationboard
   2. implement the click functionality on SbbStationList
   3. implement a SbbDepartures component to display the departures
   
### Further Exercises
* implement your own ideas
* for a example with google maps and immutableJs look at: "git checkout extendedExample"

---


# What's next?
If you're interested in building "bigger" applications with react, you should consider the following links:
* Use a library to manage your state. We recommend Redux: https://github.com/rackt/redux
* ImmutableJs to enforce unidirectional dataflow (your state should not be directly mutated by react components). Synergizes very well with Redux: https://facebook.github.io/immutable-js/
* React-Router for multiple routes: https://github.com/reactjs/react-router


# Resources

* https://facebook.github.io/react/
* https://facebook.github.io/react/docs/thinking-in-react.html
* https://github.com/rackt/redux
* https://ponyfoo.com/articles/es6
* https://babeljs.io/
