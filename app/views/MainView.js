import React from 'react';
import log from 'loglevel';

import SbbDataService from '../services/SbbDataService';
import SbbStationList from '../components/SbbStationList';

/**
 * This is an example of an "intelligent" React component.
 * it pulls together some data (from services) and stores it in it's own state.
 *
 * Parts of this state is passed down as properties to "dumb" components.
 *
 */
const MainView = React.createClass({

  getInitialState() {
    // init the view's state to an empty object, so that in the first render cycle, it is not undefined
    return {};
  },

  componentDidMount() {
    // start fetching data right away.
    // on success, we manipulate our view's state by invoking "this.setState(....)" which will trigger a new render cycle
    SbbDataService
      .searchStations('Stadelhofen')
      .then(stations => this.setState({stations: stations}))
      .catch(err => this.setState({error: err}))
      .done();
  },

  render() {

    log.debug('renderMethod in MainView is called with state %s', JSON.stringify(this.state));

    const { error, stations } = this.state;

    let content;
    if (error) {
      content = <span><b>An Error occurred! {error.message}</b></span>;
    } else {
      content = <SbbStationList stations={stations}/>;
    }

    return (
      <div className='main-view'>
        <h3><i className='fa fa-globe'></i> Hello World!</h3>

        <div>
          {content}
        </div>

      </div>
    );
  }

});

export default MainView;
